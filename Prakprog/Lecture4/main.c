#include"komplex.h"
#include"stdio.h"
#define TINY 1e-6

int main(){
	komplex a = {1,2}, b = {3,4};

	printf("testing komplex_add...\n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	printf("... and printing complex numbers using komplex_print...\n");
	komplex_print("a=",a);
	komplex_print("b=",b);
	komplex_print("a+b should   = ", R);
	komplex_print("a+b actually = ", r);
	komplex s = komplex_sub(a,b);
	komplex S = {-2,-2};
	komplex_print("a-b should   = ", S);
	komplex_print("a-b actually = ", s);
	int e = komplex_equal(a,b);
	printf("a==b should return: 0\n");
	printf("a==b actually returns: %i\n",e);
	komplex c={3,4};
	komplex_print("c=",c);
	int f = komplex_equal(c,b);
	printf("c==b should return: 1\n");
	printf("c==b actually returns: %i\n",f);
	komplex ab =komplex_mul(a,b);
	komplex_print("a*b actually = ",ab);
/* the following is optional */

/*	if( komplex_equal(R,r,TINY,TINY) )
		printf("test 'add' passed :) \n");
	else
		printf("test 'add' failed: debug me, please... \n");
*/
}
