#include<stdio.h>
#include"komplex.h"

void komplex_print (char *s, komplex a) {
	if(a.im>=0){
		printf ("%s %g + i%g\n", s, a.re, a.im);
	}
	else{
		printf ("%s %g - i%g\n", s, a.re,-1* a.im);
	}


}

komplex komplex_new (double x, double y) {
	komplex z = { x, y };
	return z;
}

void komplex_set (komplex* z, double x, double y) {
	(*z).re = x;
	(*z).im = y;
}

komplex komplex_add (komplex a, komplex b) {
	komplex result = { a.re + b.re , a.im + b.im };
	return result;
}

komplex komplex_sub (komplex a, komplex b) {
	komplex result = { a.re - b.re , a.im - b.im };
	return result;
}

int komplex_equal (komplex a, komplex b){
	if(a.re == b.re && a.im == b.im){return 1;
	}
	else{return 0;}
}

komplex komplex_mul(komplex a, komplex b){
	komplex result= {a.re * b.re, a.im * b.im};
	return result;
}
/* ... */
