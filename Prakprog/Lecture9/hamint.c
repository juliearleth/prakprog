#include <math.h>
#include <gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

double psi (double x, void * params) {
  double alpha = *(double *) params;
  double integrand = exp(-alpha * x * x);
  return integrand;
}

double hamiltonian (double x, void *params){

  double alpha = *(double *)params;
  double integrand = (-alpha * alpha * x * x + alpha + x*x)*exp(-alpha * x * x)/2;
  return integrand;

}

double integral(double alpha, double (*integrand)(double x, void *params)){

  gsl_function F;
  F.function = integrand;
  F.params = (void *)&alpha;
  double epsabs = 1e-7, epsrel = 1e-7, limit = 1000, a=0, result, error;

  gsl_integration_workspace * w = gsl_integration_workspace_alloc (limit);

  int status = gsl_integration_qagiu(&F, a, epsabs, epsrel, limit, w, &result, &error);

  gsl_integration_workspace_free(w);

  if(status!=GSL_SUCCESS) return NAN;
  else return 2*result;

}

double energy (double alpha){
  double energy = integral(alpha, &hamiltonian)/integral(alpha, &psi);
  return energy;
}

