#include "stdio.h"
#include "math.h"
#include "stdlib.h"
#include "gsl/gsl_sf_airy.h"
#include "gsl/gsl_vector.h"

int main(){
	int size = 1000;
	gsl_vector * x_val = gsl_vector_alloc(size);
	gsl_vector * airy_A = gsl_vector_alloc(size);
	gsl_vector * airy_B = gsl_vector_alloc(size);

	double val=-15;
	for (int i = 0; i < size; ++i)
	{	
		gsl_vector_set(x_val,i,val);
		val+=0.015;
		//fprintf(stderr,"%g \n", gsl_vector_get(x_val,i));
	}

	for (int i = 0; i < size; ++i)
	{	
		double A = gsl_sf_airy_Ai(gsl_vector_get(x_val,i), GSL_PREC_SINGLE);
		gsl_vector_set(airy_A,i,A);

		

		double B = gsl_sf_airy_Bi(gsl_vector_get(x_val,i), GSL_PREC_SINGLE);
		gsl_vector_set(airy_B,i,B);

		printf("%lg \t %lg \t %lg\n",gsl_vector_get(x_val,i),gsl_vector_get(airy_A,i),gsl_vector_get(airy_B,i));


	}


	gsl_vector_free(x_val);
	gsl_vector_free(airy_A);
	gsl_vector_free(airy_B);

}
