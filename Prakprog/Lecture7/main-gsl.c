#include <stdio.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_blas.h>

void matrix_print(const char* s,const gsl_matrix* m){
  printf(s); printf("\n");
  for(int j=0;j<m->size2;j++){
    for(int i=0;i<m->size1;i++)
      printf("%8.3g ",gsl_matrix_get(m,j,i));
    printf("\n");
    }
  }
void vector_print(const char* s,const gsl_vector* v){
  printf(s); printf("\n");
  for(int i=0;i<v->size;i++)
    printf("%8.3g ",gsl_vector_get(v,i));
  printf("\n");
  }
int
main (void)
{ int n = 3;
  gsl_vector *b = gsl_vector_calloc(n);
  gsl_vector_set(b, 0, 6.32);
  gsl_vector_set(b, 1, 5.37);
  gsl_vector_set(b, 2, 2.29);
  gsl_matrix *M = gsl_matrix_calloc(n, n);

  gsl_matrix_set(M, 0, 0, 6.13);
  gsl_matrix_set(M, 1, 0, 8.08);
  gsl_matrix_set(M, 2, 0, -4.36);
  gsl_matrix_set(M, 0, 1, -2.90);
  gsl_matrix_set(M, 1, 1, -6.31);
  gsl_matrix_set(M, 2, 1, 1.00);
  gsl_matrix_set(M, 0, 2, 5.86);
  gsl_matrix_set(M, 1, 2, -3.89);
  gsl_matrix_set(M, 2, 2, 0.19);

  gsl_matrix* A=gsl_matrix_calloc(n,n);
  gsl_matrix_memcpy(A,M);
  matrix_print("matrix M=",M);
  vector_print("right hand side b=",b);

  gsl_vector *x = gsl_vector_alloc (n);

  int s;

  gsl_permutation * p = gsl_permutation_alloc (n);

  gsl_linalg_LU_decomp (M, p, &s);

  gsl_linalg_LU_solve (M, p, b, x);


  vector_print("solution x to Mx=b :",x);
  gsl_vector* y=gsl_vector_calloc(n);
  gsl_blas_dgemv(CblasNoTrans,1,A,x,0,y);
  vector_print("check:  Mx= (should be equal to b)",y);



  gsl_permutation_free (p);
  gsl_vector_free (x);
  gsl_vector_free(b);
  gsl_matrix_free(M);
  gsl_matrix_free(A);
  return 0;
}
