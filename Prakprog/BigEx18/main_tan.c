#include <stdlib.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>
#include <math.h>
#include "mytan.h"
double rootfinder (double z);

int main(){
  printf("i myarctan(i) arctan(i)\n");
  for(double i=-2.0;i<=2;i+=0.05)
  printf("%8.3f %8.16f %8.16f\n",i,rootfinder(i),atan(i));
}