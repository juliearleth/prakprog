#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>
#include <math.h>

int mytan (const gsl_vector * v, void *params, gsl_vector * f){
  double x = gsl_vector_get(v, 0);
  double a = *(double*) params;
  gsl_vector_set(f, 0, a - tan(x));

  return GSL_SUCCESS;
}

