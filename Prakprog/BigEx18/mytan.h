#include <stdlib.h>
#include <stdio.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>

struct rparams{
    double a;
  };

int mytan (const gsl_vector * v, void *params, gsl_vector * f);
int print_state (size_t iter, gsl_multiroot_fsolver * s);