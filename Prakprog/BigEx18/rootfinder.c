#include <stdlib.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>
#include "mytan.h"

int rootfinder (double a)
{
  const gsl_multiroot_fsolver_type *T;
  gsl_multiroot_fsolver *s;

  int status;
  size_t iter = 0;

  
  gsl_multiroot_function f;
  f.f = &mytan;
  f.n = 1;
  f.params = (void*)&a;

  double x_init = 1.0;
  double n = 1;
  gsl_vector *x = gsl_vector_alloc (n);

  gsl_vector_set (x, 0, x_init);
  
  T = gsl_multiroot_fsolver_hybrids;
  s = gsl_multiroot_fsolver_alloc (T, 1);
  gsl_multiroot_fsolver_set (s, &f, x);


  do
    {
      iter++;
      status = gsl_multiroot_fsolver_iterate (s);

      if (status)   /* check if solver is stuck */
        break;

      status =
        gsl_multiroot_test_residual (s->f, 1e-7);
    }
  while (status == GSL_CONTINUE && iter < 1000);

  double result = gsl_vector_get (s->x, 0);

  gsl_multiroot_fsolver_free (s);
  gsl_vector_free (x);
  return result;
}
