#include <stdio.h>
#include <tgmath.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv2.h>
#include <getopt.h>

int error_fun(double x, const double y[], double f[], void *params)
{
	f[0] = y[1];
	f[0] = 2/sqrt(M_PI) * exp(-x*x);
	return GSL_SUCCESS;
}

double ode_solve(double x){
	gsl_odeiv2_system sys;
	sys.function = error_fun;
	sys.jacobian = NULL;
	sys.dimension = 1;
	sys.params = NULL;

	double acc=1e-6,eps=1e-6,hstart=copysign(1e-3,x);
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new
		(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

	double t=0,y[1]={0};
	gsl_odeiv2_driver_apply(driver,&t,x,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}

int main(int argc, char *argv[]){
	double a = 0, b = 0,dx=0;
	while(1){
	int opt = getopt(argc, argv, "a:b:d:");
	if(opt == -1) break;
		switch (opt)
		{
		case 'a':
			a = atof(optarg);
			break;
		case 'b':
			b = atof(optarg);
			break;
		case 'd':
			dx = atof(optarg);
			break;
		default: /* ? */ //
			fprintf(stderr, "Usage: %s [-a lower bound] [-b upper bound] [-d dx~interval]\n", argv[0]);
			exit(EXIT_FAILURE);
		}
	}
	
	for(double x=a;x<b+dx;x+=dx)printf("%g %g %g\n",x, ode_solve(x), erf(x));
return 0;
}