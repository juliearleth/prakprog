#include<stdio.h>
#include<stdlib.h>
#include"nvector.h"

nvector* nvector_alloc(int n){
  nvector* v = malloc(sizeof(nvector));
  (*v).size = n;
  (*v).data = malloc(n*sizeof(double));
  if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
  return v;
}

void nvector_free(nvector* v){ free(v->data); free(v);}

void nvector_set(nvector* v, int i, double value){ (*v).data[i]=value; }

double nvector_get(nvector* v, int i){return (*v).data[i]; }

void nvector_add(nvector* a,nvector* b){
	if(a->size == b->size)
		for(int i=0;i<a->size;i++){
			double s = nvector_get(a,i) +nvector_get(b,i);
			nvector_set(a,i,s);
		}
}

void nvector_print(char *s, nvector* v)
{
	printf("%s", s);
	for (int i = 0; i < v->size; i++)
		printf("%9.3g ", v->data[i]);
	printf("\n");
}

double nvector_dot_product(nvector* a,nvector* b){
	double s=0;
	if(a->size == b->size){
		for(int i=0;i<a->size;i++){
			double value =nvector_get(a,i)*nvector_get(b,i);
			s += value;

		}
		
	}else{
		printf("Error: vector sizes not equal.\n");
	}

	return s;
}

void nvector_sub(nvector* a, nvector* b){
	if(a->size == b->size)
		for (int i = 0; i < a->size; ++i)
		{
			double s=nvector_get(a,i)- nvector_get(b,i);
			nvector_set(a,i,s);
		}
	
}

void nvector_set_zero(nvector* v){
	for (int i = 0; i < v->size; ++i)
	{
		nvector_set(v,i,0);
	}
}
/* ... */