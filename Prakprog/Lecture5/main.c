#include "nvector.h"
#include "stdio.h"
#include "stdlib.h"
#define RND (double)rand()/RAND_MAX

int main()
{
	int n = 5;

	printf("\nmain: testing nvector_alloc ...\n");
	nvector *v = nvector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n");

	printf("\nmain: testing nvector_set and nvector_get ...\n");
	double value = RND;
	int i = n / 2;
	nvector_set(v, i, value);
	double vi = nvector_get(v, i);
	if (vi==value) printf("test passed\n");
	else printf("test failed\n");



	printf("\nmain: testing nvector_add ...\n");
	nvector *a = nvector_alloc(n);
	nvector *b = nvector_alloc(n);
	nvector *c = nvector_alloc(n);
	nvector *d = nvector_alloc(n);
	nvector *g = nvector_alloc(n);
	for (int i = 0; i < n; i++) {
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(g, i, x);
		nvector_set(b, i, y);
		nvector_set(c, i, x + y);
		nvector_set(d, i, x - y);
	}
	nvector_add(a, b);
	nvector_print("a+b should   = ", c);
	nvector_print("a+b actually = ", a);

	printf("main: testing nvector_dot_product ...\n");

	nvector *e = nvector_alloc(2);
	nvector *f = nvector_alloc(2);
	nvector_set(e,0,2.0f);
	nvector_set(e,1,3.0f);
	nvector_set(f,0,4.0f);
	nvector_set(f,1,5.0f);
	double k = 2.0*4.0 + 3.0*5.0;

	double s=nvector_dot_product(e,f);
	nvector_print("e= ",e);
	nvector_print("f= ",f);
	printf("e*f should   = %g\n" ,k);
	printf("e*f actually = %g\n" ,s);

	printf("main: testing nvector_set_zero ...\n");
	nvector_print("e before test: ",e);
	nvector_set_zero(e);
	nvector_print("e after test:  ",e);

	nvector_print("g=", g);
	nvector_print("b=", b);
	nvector_sub(g, b);
	nvector_print("g-b should   = ", d);
	nvector_print("g-b actually = ", g);

/*
	if (nvector_equal(c, a)) printf("test passed\n");
	else printf("test failed\n");
*/
	nvector_free(v);
	nvector_free(a);
	nvector_free(b);
	nvector_free(c);
	nvector_free(d);
	nvector_free(e);
	nvector_free(f);

	return 0;
}