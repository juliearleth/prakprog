
main: testing nvector_alloc ...
test passed

main: testing nvector_set and nvector_get ...
test passed

main: testing nvector_add ...
a+b should   =      1.18      1.71     0.533      1.05      1.03 
a+b actually =      1.18      1.71     0.533      1.05      1.03 
main: testing nvector_dot_product ...
e=         2         3 
f=         4         5 
e*f should   = 23
e*f actually = 23
main: testing nvector_set_zero ...
e before test:         2         3 
e after test:          0         0 
g=    0.394     0.798     0.198     0.768     0.554 
b=    0.783     0.912     0.335     0.278     0.477 
g-b should   =    -0.389    -0.113    -0.138      0.49    0.0766 
g-b actually =    -0.389    -0.113    -0.138      0.49    0.0766 
