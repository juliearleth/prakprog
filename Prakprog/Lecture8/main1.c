#include <stdio.h>
#include <math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_odeiv2.h>

int diff_eq(double x, const double y[], double y_prime[]){
	y_prime[0] = y[0]*(1-y[0]);
	return GSL_SUCCESS;
}

int main(int argc, char const *argv[])
{
	// Part 1: Solve y'(x) = y(x)*(1-y(x)), from x=0 to x=3 with y(0)=0.5
	gsl_odeiv2_system sys;
	sys.function = diff_eq;
	sys.jacobian = NULL;
	sys.dimension = 1;

	double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;

	gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);

	double delta_x = 0.05;
	double y = 0.5;
	double t = 0;
	for (double x = 0; x < 3; x+=delta_x)
	{
		int status = gsl_odeiv2_driver_apply (driver, &t, x, &y);
		printf ("%g \t %g\n", x, y);
		if (status != GSL_SUCCESS) fprintf (stderr, "fun: status=%i", status);
	}
	
	gsl_odeiv2_driver_free (driver);
	return 0;
}