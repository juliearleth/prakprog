#include<math.h>
#include<complex.h>
#include<stdio.h>

int main () {
	double x=5;
	double gx=tgamma(x);
	printf("gamma(%g)=%g\n\n",x,gx);

	double y=0.5;
	double Jy=j1(y);
	printf("J_1(%g)=%g\n\n",y,Jy);	

	double z=-2;
	complex sqrz=csqrt(z);
	printf("Sqrt(%g)=%g + i*%g\n\n",z,creal(sqrz),cimag(sqrz));

	complex expi=cexp(1.0*I);
	printf("exp(i)=%g + i*%g\n\n",creal(expi),cimag(expi));

	complex exppii=cexpl(M_PI*I);
	printf("exp(pi*i)=%Lg +i*%Lg \n\n",creall(exppii),cimagl(exppii));

	complex ie=cpow(I,M_E);
	printf("i^e=%g + i*%g \n\n",creal(ie),cimag(ie));

	printf("PART TWO\n\n");

	float h= 0.1111111111111111111111111111;
	double j= 0.1111111111111111111111111111;
	long double k= 0.1111111111111111111111111111L;
	printf("float:		%.25g\ndouble:		%.25lg\nlong double:	%.25Lg \n\n",h,j,k);

return 0;


}

