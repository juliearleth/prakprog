#include <stdio.h>
int equal(double a, double b, double tau, double epsilon);

int main(int argc, char const *argv[])
{
	printf("\n--------3: Equal--------\n\n");
	double a = 3.45, b = 3.50, tau = 0.01, epsilon = 0.01;
	char yes[] = "Yes.";
  	char no[] = "No.";
  	printf("tau =%g and epsilon = %g\n",tau, epsilon );
  	int isequal = equal(a, b, tau, epsilon);
  	if (isequal==1) {
    	printf("Are %g and %g equal? %s\n\n", a, b, yes);
  	} else if (isequal==0) {
    	printf("Are %g and %g equal? %s\n\n", a, b, no);
  	} else {
    	printf("Error\n\n");
	}

	tau = 0.01, epsilon = 0.05;
  	printf("tau =%g and epsilon = %g\n",tau, epsilon );
  	isequal = equal(a, b, tau, epsilon);
  	if (isequal==1) {
    	printf("Are %g and %g equal? %s\n\n", a, b, yes);
  	} else if (isequal==0) {
    	printf("Are %g and %g equal? %s\n\n", a, b, no);
  	} else {
    	printf("Error\n\n");
	}

	tau = 0.05, epsilon = 0.01;
  	printf("tau =%g and epsilon = %g\n",tau, epsilon );
  	isequal = equal(a, b, tau, epsilon);
  	if (isequal==1) {
    	printf("Are %g and %g equal? %s\n\n", a, b, yes);
  	} else if (isequal==0) {
    	printf("Are %g and %g equal? %s\n\n", a, b, no);
  	} else {
    	printf("Error\n\n");
	}

	return 0;
}