
#include<limits.h>
#include<stdio.h>
#include<float.h>

int main(){
	printf("--------1i: INT_MAX--------\n\n");
	int i=1; while(i+1>i){i++;}
	int max=INT_MAX;
	int testi=i-max;
	printf("While:\t\tMy max int=%i\tDifference from INT_MAX: %i\n", i, testi);
	

	int j=1; for(int i=1;i+1>i;i++){j++;}
	int testj=j-max;
	printf("For:\t\tMy max int=%i\tDifference from INT_MAX: %i\n", j, testj);

	int k=1;
	do
	{
		k++;
		
	}while(k+1>k);
	int testk=k-max;
	printf("Do while:\tMy max int=%i\tDifference from INT_MAX: %i\n\n", k, testk);

	
	printf("--------1ii: INT_MIN--------\n\n");
	i=1;
	while(i-1<i){i--;}
	int min=INT_MIN;
        testi=i-min;
	printf("While:\t\tMy min int=%i\tDifference from INT_MIN: %i\n", i, testi);

	j=1;
	for(int i=1;i-1<i;i--){j--;}
	testj=j-min;
	printf("For:\t\tMy min int=%i\tDifference from INT_MIN: %i\n", j, testj);

	k=1;
	do{
	k--;
	}while(k-1<k);
	testk=k-min;
	printf("Do while:\tMy min int=%i\tDifference from INT_MIN: %i\n\n", k, testk);
        
       
	printf("--------1iii: MACHINE EPSILON--------\n\n");

	printf("----WHILE LOOPS----\n\n");

	float x=1; while(1+x!=1){x/=2;} x*=2;
	float testx=x-FLT_EPSILON;
	printf("Float\t\tmachine epsilon=%g\tDifference from defined:%g\n",x,testx);


	double y=1; while(1+y!=1){y/=2;} y*=2;
    double testy=y-DBL_EPSILON;
	printf("Double\t\tmachine epsilon=%g\tDifference from defined:%g\n",y, testy);

	long double z=1; while(1+z!=1){z/=2;} z*=2;
    long double testz=z-LDBL_EPSILON;
	printf("Long double\tmachine epsilon=%Lg\tDifference from defined:%Lg\n\n",z,testz);

	printf("----FOR LOOPS----\n\n");

	float ex=1; for(ex=1;1+ex!=1;ex/=2){} ex*=2;
	float testex=ex-FLT_EPSILON;
	printf("Float\t\tmachine epsilon=%g\tDifference from defined:%g\n",ex,testex);
	

	double ey=1; for(ey=1;1+ey!=1;ey/=2){} ey*=2;
	double testey=ey-DBL_EPSILON;
    printf("Double\t\tmachine epsilon=%g\tDifference from defined:%g\n",ey,testey);

	long double ez=1; for(ez=1;1+ez!=1;ez/=2){} ez*=2;
    long double testez=ez-LDBL_EPSILON;
    printf("Long double\tmachine epsilon=%Lg\tDifference from defined:%Lg\n\n",ez, testez);
        
	printf("----DO WHILE LOOPS----\n\n");

	ex=1; do{ex/=2;}while(1+ex!=1); ex*=2;
	testex=ex-FLT_EPSILON;
	printf("Float\t\tmachine epsilon=%g\tDifference from defined:%g\n",ex, testex);

	ey=1; do{ey/=2;}while(1+ey!=1); ey*=2;
	testy=ey-DBL_EPSILON;
	printf("Double\t\tmachine epsilon=%g\tDifference from defined:%g\n",ey,testey);

	ez=1; do{ez/=2;}while(1+ez!=1); ez*=2;
	testez=ez-LDBL_EPSILON;
	printf("Long double\tmachine epsilon=%Lg\tDifference from defined:%Lg\n\n",ez,testez);


	printf("--------2i: Sums--------\n\n");
	
	max=INT_MAX/2;
	float sum_up_float=0;
	for(int i=1;i<=max;i++){sum_up_float=sum_up_float+1.0f/i;}
	printf("The sum up float is equal to:\t\t%g\n",sum_up_float);

	float sum_down_float=0;
	for(int i=max;i>=1;i--){sum_down_float=sum_down_float+1.0f/i;}
	printf("The sum down float is equal to:\t\t%g\n\n",sum_down_float);

	printf("2ii) \nThe difference between the two sums is due to float precision ignoring small numbers added to a large number.\n\n");

	printf("2iii)\nDoes the sum converge as a function of max? \nNo, rather as a function of precision.\n\n");
	double sum_up_double=0;
	for(int i=1;i<=max;i++){sum_up_double=sum_up_double+1.0/i;}
	printf("The sum up double is equal to:\t\t%.20g\n",sum_up_double);

	double sum_down_double=0;
	for(int i=max;i>=1;i--){sum_down_double=sum_down_double+1.0/i;}
	printf("The sum down double is equal to:\t%.20g\n",sum_down_double);
	


return 0;


}
