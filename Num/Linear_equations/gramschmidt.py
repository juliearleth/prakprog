import numpy as np
import numpy.matlib


def qr(A: np.matrix):
    m = A.shape[1]
    Q = A.copy()
    R = np.matlib.zeros((m, m))
    for i in range(m):
        R[i, i] = np.linalg.norm((Q[:, i]))
        Q[:, i] /= R[i, i]
        for j in range(i + 1, m):
            R[i, j] = np.dot(np.transpose(Q[:, i]), Q[:, j])
            Q[:, j] -= R[i, j] * Q[:, i]
    return Q, R

def qr_solve(Q: np.matrix, R: np.matrix, b: np.array):
    c = np.dot(np.transpose(Q), b)
    n = len(c)
    x = np.zeros(n)

    for i in reversed(range(n)):
        c_i = c[i]
        for j in range(i+1, n):
            c_i -= R[i, j] * x[j]

        x[i] = c_i / R[i, i]
    return x
def qr_inverse(Q:np.matrix, R:np.matrix):
    n = Q.shape[0]
    B = np.eye(n)
    A_inv = np.empty((n, n))
    for i in range(n):
        A_inv[:,i] = qr_solve(Q, R, B[:][i])
    return A_inv