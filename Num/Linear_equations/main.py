import random
import gramschmidt
import numpy as np
import numpy.matlib


if __name__ == '__main__':
    print("\n1) QR decomposition:")
    eps = 1e-15
    n = 5
    m = 4
    A = np.matlib.zeros((n, m))
    for i in range(0, A.shape[0]):
        for j in range(0, A.shape[1]):
            A[i, j] = (1 + i + j) * random.random()
    print("A random matrix A:\n {}".format(A))
    A2 = A.copy()
    QR = gramschmidt.qr(A2)
    print("Q=\n {} \n R= \n {}".format(QR[0],QR[1]))
    Q=QR[0]
    QTQ = np.matrix.transpose(Q)*Q
    print("Testing if QTQ=I...")
    if np.allclose(QTQ, np.identity(m), rtol=eps, atol=eps):
        print("QTQ=\n {} \nTest successful within tolerance {}".format(QTQ, eps))
    else:
        print("Test failed")
    R=QR[1]
    AQR=Q*R
    print("Testing if Q*R=A...")
    if np.allclose(AQR, A2):
        print("Test successful! \n A= \n {} \n Q*R= \n {}".format(A2, AQR))
    else:
        print("Test failed")
    print("\n2) Linear Equations:")
    n=4
    B=np.random.rand(n, n) * 10
    print("A random matrix A:\n {}".format(B))
    Ac = B.copy()
    b=np.random.rand(n) * 5
    print("Random vector b:\n {}".format(b))
    QR_both=gramschmidt.qr(B)
    x = gramschmidt.qr_solve(QR_both[0], QR_both[1], b)
    print("Solution x to Ax=b:\n {}".format(x))
    print("test: A*x=b? :\n {}".format(np.dot(Ac, x)))
    if np.allclose(np.dot(Ac, x), b):
        print("\ntest Ax==b passed :) ")
    else:
        print("test Ax==b failed :( ")

    print("\n----------Ex B------------\nMatrix inverse by Gram-Schmidt QR factorization\n\n")
    m = 3
    A = np.random.rand(m, m) * 5
    print ("A = \n{}".format(A))
    print("Factorizing A into QR...")
    (Q, R) = gramschmidt.qr(A)
    print("Q=\n {} \n R= \n {}".format(Q,R))
    print("Calculating inverse B...")
    B = gramschmidt.qr_inverse(Q, R)
    print("B = \n{}".format(B))
    print("Testing if AB = I...")
    AB = np.matmul(A, B)
    if np.allclose(AB, np.identity(m), rtol=eps, atol=eps):
        print("AB=\n {} \nTest successful within tolerance {}".format(QTQ, eps))
    else:
        print("Test failed... AB = \n{}".format(AB))



