from math import *
import rk
import numpy as np

def F(t, y):
	return np.array([y[1], -y[0]])

if __name__ == '__main__':
    a = 0
    b = 4 * pi
    eps = 0.01
    acc = 0.01
    hstart = 0.1
    x = np.array([a])
    y = []
    y.append(np.array([0, 1]))
    (tlist, ylist) = rk.rkdriver(F, x, y, b, eps, acc)
    print("# m=0, S=4")
    for i in range(len(tlist)): print(tlist[i], ylist[i][0])
    #
    print("\n# m=0, S=6")
    for i in range(len(tlist)): print(tlist[i], ylist[i][1])

