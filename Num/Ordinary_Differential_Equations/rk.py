import math
import numpy as np

def rkstep12(F:'right-hand side', t:float, y:np.array, h:'step'):
    k0 = F(t, y)
    k12 = F(t+np.ones(t.size)*h/2, y+k0*(h/2))
    yh = y + k12*h
    err = np.linalg.norm((k0-k12)*(h/2))
    return (yh, err)

def rkdriver(F, tlist, ylist, b:float, h:float=1e-2, eps:float=1e-6, acc:float=1e-6):
    a = tlist[-1]
    nsteps = 0
    while nsteps <1000:
        t = tlist[-1]
        y = ylist[-1]
        if t>=b: break
        if t+h>b: h=b-t
        (yh, err) = rkstep12(F, t, y, h)
        tol = (acc + np.linalg.norm(yh)*eps)*math.sqrt(h/(b-a))
        if err < tol:
            nsteps +=1
            tlist = np.append(tlist,t+h)
            ylist.append(yh)
        if err==0 : h *= 2
        else      : h *= math.pow(tol/err, 0.25)*0.95
    return (tlist, ylist)
