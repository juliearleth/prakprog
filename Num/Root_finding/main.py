import math
import numpy as np
import newton
import scipy.optimize as opt
ncalls = 0

def rosenbrock(p):
    global ncalls
    ncalls += 1
    z = np.empty(2)
    x = p[0]; y = p[1]
    z[0] = 2*(1-x)*(-1)+100*2*(y-x*x)*(-1)*2*x
    z[1] = 100*2*(y-x*x)
    return z
def rosenbrock_jacobian(p):
    global ncalls
    ncalls += 1
    x = p[0]; y = p[1]
    J = np.empty((2, 2))
    J[0, 0] = 2*(600*x*x - 200*y +1)
    J[0, 1] = -400*x
    J[1, 0] = J[0, 1]
    J[1, 1] = 200
    return J

hcalls = 0
def himmelblau(p):
    global hcalls
    hcalls += 1
    z = np.empty(2)
    x = p[0]; y = p[1]
    z[0] = 4 * (x * x + y - 11) * x + 2 * (x + y * y - 7)
    z[1] = 2 * (x * x + y - 11) + 4 * (x + y * y - 7) * y
    return z
def himmelblau_jacobian(p):
    global hcalls
    hcalls += 1
    J = np.empty((2, 2))
    x = p[0]
    y = p[1]
    J[0, 0] = 12 * x * x + 4 * y - 42
    J[0, 1] = 4 * x + 4 * y
    J[1, 0] = 4 * x + 4 * y
    J[1, 1] = 4 * x + 12 * y * y - 26
    return J

scalls = 0
def eqsys(p):
    A = 10000
    global scalls
    scalls += 1
    z = np.empty(2)
    x = p[0];y = p[1]
    z[0] = A*x*y - 1
    z[1] = math.exp(-x) + math.exp(-y) - (1 + 1/A)
    return z
def eqsys_jacobian(p):
    A = 10000
    global scalls
    scalls += 1
    J = np.empty((2, 2))
    x = p[0];y = p[1]
    J[0, 0] = A * y
    J[0, 1] = A * x
    J[1, 0] = math.exp(-x) * (-1)
    J[1, 1] = math.exp(-y) * (-1)
    return J

dx = 1e-7
eps = 1e-5
def main_A():
    global x0
    x0 = np.array([2, 1])
    print("-----------------------Exercise A--------------------")
    print("Finding the minimum of the Rosenbrock function:")
    newton.newton_time_print(rosenbrock, x0, eps=eps, dx=dx)
    print("Calls:             {}\n".format(ncalls))
    x0 = np.array([5, 10])
    print("Finding the minimum of the Himmelblau function:")
    newton.newton_time_print(himmelblau, x0, eps=eps, dx=dx)
    print("Calls:             {}\n".format(hcalls))
    x0 = np.array([2, 1])
    print("Finding the solution to the system of equations:")
    newton.newton_time_print(eqsys, x0, eps=eps, dx=dx)
    print("Calls:             {}\n".format(scalls))





def main_B1():
    global x0
    print("----------Exercise B--------------------------------")
    print("Providing an analytic Jacobian\n\n")
    print("Finding the minimum of the Rosenbrock function:")
    newton.newton_time_print_with_jacobian(rosenbrock, rosenbrock_jacobian, x0, eps=eps)
    print("Calls:             {}\n".format(ncalls))
    x0 = np.array([5, 10])
    print("Finding the minimum of the Himmelblau function:")
    newton.newton_time_print_with_jacobian(himmelblau, himmelblau_jacobian, x0, eps=eps)
    print("Calls:             {}\n".format(hcalls))
    x0 = np.array([2, 1])
    print("Finding the solution to the system of equations:")
    newton.newton_time_print_with_jacobian(eqsys, eqsys_jacobian, x0, eps=eps)
    print("Calls:             {}\n".format(scalls))




def main_B2():
    global x0
    print("-------- Root finding using scipy.optimize.root------\n")
    print("The Rosenbrock function:")
    newton.sp_time_print(rosenbrock, x0, 5000)
    print("Calls:             {}\n".format(ncalls))
    x0 = np.array([5, 10])
    print("The Himmelblau function:")
    newton.sp_time_print(himmelblau, x0, 1000)
    print("Calls:             {}\n".format(hcalls))
    x0 = np.array([2, 1])
    print("The system of equations:")
    newton.sp_time_print(eqsys, x0, 1000)
    print("Calls:             {}\n\n".format(scalls))




def main_C():
    global x0
    # ncalls = hcalls = scalls = 0
    print("---------------Exercise C---------------------")
    print("Using quadratic spline linesearch\n\n")
    x0 = np.array([2, 1])
    print("Finding the minimum of the Rosenbrock function:")
    newton.newton_time_print_qline(rosenbrock, x0, eps=eps, dx=dx)
    print("Calls:             {}\n".format(ncalls))
    x0 = np.array([5, 10])
    print("Finding the minimum of the Himmelblau function:")
    newton.newton_time_print_qline(himmelblau, x0, eps=eps, dx=dx)
    print("Calls:             {}\n".format(hcalls))
    x0 = np.array([2, 1])
    print("Finding the solution to the system of equations:")
    newton.newton_time_print_qline(eqsys, x0, eps=eps, dx=dx)
    print("Calls:             {}\n".format(scalls))



if __name__ == '__main__':
    main_A()
    ncalls = scalls = hcalls = 0
    main_B1()
    ncalls = scalls = hcalls = 0
    main_B2()
    ncalls = scalls = hcalls = 0
    main_C()