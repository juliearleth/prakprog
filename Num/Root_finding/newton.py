import sys, time
sys.path.append('../Linear_equations')
import gramschmidt as gs
import numpy as np
import scipy.optimize as opt


def newton(f:"function", xstart:np.array, eps:float, dx:float):
    x = xstart.copy().astype(float)
    # print("xstart={}".format(xstart))
    # print("f(xstart)={}".format(f(xstart)))
    n = x.size
    J = np.empty([n, n])
    steps = 0
    while True:
        steps += 1
        fx = f(x)
        for j in range(n):
            x[j] += dx
            df = f(x) - fx
            for i in range(n): J[i, j] = df[i] / dx
            x[j] -= dx
        (Q, R) = gs.qr(J)
        Dx = gs.qr_solve(Q, R, -fx)
        s = 2
        while True:
            s /= 2
            y = x + Dx * s
            fy = f(y)
            if np.linalg.norm(fy) < (1 - s / 2) * np.linalg.norm(fx) or s < 0.02: break
        x = y
        fx = fy
        if np.linalg.norm(Dx) < dx or np.linalg.norm(fx) < eps: break
    return x, steps

def newton_with_jacobian(f:"function", jacobian:"function", xstart:np.array, eps:float):
    x = xstart.copy().astype(float)
    n = x.size
    J = np.empty([n, n])
    steps = 0
    while True:
        steps += 1
        fx = f(x)
        J = jacobian(x)
        (Q, R) = gs.qr(J)
        Dx = gs.qr_solve(Q, R, -fx)
        s = 2
        while True:
            s /= 2
            y = x + Dx * s
            fy = f(y)
            if np.linalg.norm(fy) < (1 - s / 2) * np.linalg.norm(fx) or s < 0.02: break
        x = y
        fx = fy
        if np.linalg.norm(fx) < eps: break
    return x, steps

def newton_qline(f:"function", xstart:np.array, eps:float, dx:float):
    x = xstart.copy().astype(float)
    # print("xstart={}".format(xstart))
    # print("f(xstart)={}".format(f(xstart)))
    n = x.size
    J = np.empty([n, n])
    steps = 0
    while True:
        steps += 1
        fx = f(x)
        for j in range(n):
            x[j] += dx
            df = f(x) - fx
            for i in range(n): J[i, j] = df[i] / dx
            x[j] -= dx
        # print("hej1")
        (Q, R) = gs.qr(J)
        Dx = gs.qr_solve(Q, R, -fx)
        g0 = 0.5 * (np.linalg.norm(fx))**2
        dg0 = -(np.linalg.norm(fx))**2
        s = 1
        y = x + Dx*s
        fy = f(y)
        while True:
            # print("hej")
            gsl = 0.5*(np.linalg.norm(fy))**2
            c = (gsl - g0 - dg0*s)/(s*s)
            s = - dg0/(2*c)
            y = x + Dx * s
            if np.linalg.norm(fy) < (1 - s / 2) * np.linalg.norm(fx) or s < 0.02: break
        # print("hej3")
        x = y
        fx = fy
        if np.linalg.norm(Dx) < dx or np.linalg.norm(fx) < eps: break
    return x, steps

def newton_time_print(f:"function", xstart:np.array, eps:float, dx:float):
    start = time.time()
    (b, steps) = newton(f, xstart, eps, dx)
    stop = time.time()
    print("Initial vector x0: {}".format(xstart))
    print("Gradient(x0):      {}".format(f(xstart)))
    print("Solution x:        {}".format(b))
    print("Gradient(x):       {} < {}".format(f(b), eps))
    # print("Calls:             {}".format(ncalls))
    print("Steps:             {}".format(steps))
    print("Time:              {}".format(stop - start))

def newton_time_print_with_jacobian(f:"function", jacobian:"function", xstart:np.array, eps:float):
    start = time.time()
    (b, steps) = newton_with_jacobian(f, jacobian, xstart, eps)
    stop = time.time()
    print("Initial vector x0: {}".format(xstart))
    print("Gradient(x0):      {}".format(f(xstart)))
    print("Solution x:        {}".format(b))
    print("Gradient(x):       {} < {}".format(f(b), eps))
    # print("Calls:             {}".format(ncalls))
    print("Steps:             {}".format(steps))
    print("Time:              {}".format(stop - start))

def newton_time_print_qline(f:"function", xstart:np.array, eps:float, dx:float):
    start = time.time()
    (b, steps) = newton_qline(f, xstart, eps, dx)
    stop = time.time()
    print("Initial vector x0: {}".format(xstart))
    print("Gradient(x0):      {}".format(f(xstart)))
    print("Solution x:        {}".format(b))
    print("Gradient(x):       {} < {}".format(f(b), eps))
    # print("Calls:             {}".format(ncalls))
    print("Steps:             {}".format(steps))
    print("Time:              {}".format(stop - start))

def sp_time_print(f:"function", xstart, maxiter:int):
    start = time.time()
    b = opt.root(f, xstart, method='broyden1', options={"maxiter": maxiter})
    stop = time.time()
    if b.success == False:
        print('Error! The maximum number of iterations has been reached: {}'.format(b.nit))
    else:
        print("Initial vector x0: {}".format(xstart))
        print("Gradient(x0):      {}".format(f(xstart)))
        print("Solution x:        {}".format(b.x))
        print("Gradient(x):       {}".format(f(b.x)))
        print("Steps:             {}".format(b.nit))
        # print("Function calls:    {}".format(ncalls))
        print("Time:              {}".format(stop - start))