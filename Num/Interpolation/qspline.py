import math
import numpy as np

def qspline(x:np.array, y:np.array):
    n = len(x)
    Dx = np.zeros(n-1)
    slope = np.zeros(n-1)
    c = np.zeros(n-1)
    b = np.zeros(n-1)

    for i in range(n-1):
        Dx[i] = x[i+1] - x[i]
        slope[i] = (y[i+1] - y[i])/Dx[i]

    for i in range(n-2): #recursion up
        c[i+1] = (slope[i+1] - slope[i] - c[i] * Dx[i]) / Dx[i+1]

    c[n-2] /= 2
    for i in reversed(range(n-2)): #recursion down
        c[i] = (slope[i+1] - slope[i] - c[i+1] * Dx[i+1]) / Dx[i]

    for i in range(n-1):
        b[i] = slope[i] - c[i]*Dx[i]

    def qspline_eval(z, deriv:int=0):
        assert z >= x[0] and z <= x[-1]
        i = 0
        j = n-1 
        while (j - i) > 1:
            mid = math.floor((i+j)/2)
            if z > x[mid]: i = mid
            else: j = mid
        dx = z-x[i]
        if deriv == 1: return b[i] + 2*c[i]*dx # return derivative
        elif deriv == -1:
            s = 0
            for k in range(i): s += y[k] * Dx[k] + b[k]*Dx[k]**2/2 + c[k]*Dx[k]**3/3
            s += y[i]*dx + b[i]*dx**2/2 + c[i]*dx**3/3
            return s                #return integral
        else: return y[i] + (slope[i] + c[i]*(z - x[i+1]))*(z - x[i]) #return function
    return qspline_eval
        
