import math
import random

def linterp(x:list, y:list, z:float):
   assert len(x) == len(y)
   i = 0;
   j = len(x) - 1
   while j - i > 1:
      m = math.floor((i + j) / 2)
      if z > x[m]:
         i = m
      else:
         j = m
   return y[i] + (z - x[i]) * (y[i + 1] - y[i]) / (x[i + 1] - x[i])


def linterp_integ(x:list, y:list, Z:float):
    assert len(x) == len(y)
    # assert z >= x[0] && z <= x[-2];
    current_index = 0
    next_index = 1
    current_x = x[current_index]
    next_x = x[next_index]
    value = 0
    while next_x < z:
        bi = (y[next_index] - y[current_index]) / (next_x - current_x)
        value += y[current_index] * (next_x - current_x) + bi * ((next_x ** 2 -current_x ** 2)/2 - current_x * next_x + current_x ** 2)
        current_index += 1
        next_index += 1
        current_x = x[current_index]
        next_x = x[next_index]
    bi = (y[next_index] - y[current_index]) / (next_x - current_x)
    value += y[current_index] * (z-current_x) + bi * ((z ** 2 - current_x ** 2)/2 -current_x * z + current_x ** 2)
    return value

if __name__ == '__main__':
    N = 5;
    n = 100
    x = [i + 0.5 * random.random() for i in range(N)]
    y = [math.sin(0.6 * t) for t in x]
    step = (x[-1] - x[0]) / n

    # print("#Points")
    for i in range(len(x)): print("{:6}\t{:6}".format(x[i],y[i]))
    print("\n")
    # print("\n#Interpolated line")
    z = x[0]
    while z <= x[-1]:
        print("{:6}\t{:6}\t{:6}".format(z,linterp(x,y,z),linterp_integ(x,y,z)))
        z = z + step
