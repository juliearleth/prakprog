import qspline as qs
import numpy as np
from math import *
import random

if __name__ == '__main__':
    N = 20
    n = 500
    x = [i/pi for i in range(N)]
    y = [cos(t) for t in x]
    step = (x[-1]-x[0])/n

    for i in range(N):
        print("{}   {}   {}   {}".format(x[i], y[i], sin(x[i]), -sin(x[i]))) #x, function, integral, derivative

    print("\n")

    q = qs.qspline(x, y)
    z = x[0]
    while z <= x[-1]:
        print("{}   {}   {}   {}".format(z, q(z, 0), q(z, -1), q(z, 1))) #z, function, integral, derivative
        z += step