import numpy as np
import scipy.optimize as opt


class Neural_Network:
    def __init__(self, num_neurons, function):
        self.n = num_neurons
        self.f = function
        self.params = np.empty([self.n, 5])

    def feed_forward(self, x, y):
        # x =input[0]
        # y =input[1]
        output = 0
        for i in range(self.n):
            a = self.params[i][0]
            b = self.params[i][1]
            c = self.params[i][2]
            d = self.params[i][3]
            w = self.params[i][4]
            output += self.f((x + a)/b, (y + c)/d)*w
        return output

    def train(self, x, y, exact_values:np.array):
        def delta(p):
            self.params = np.reshape(p, (self.n, 5))
            f = exact_values
            fy = self.feed_forward(x, y)
            s = np.sum(np.abs(fy-f)**2)
            return s

        opt.minimize(delta, np.random.rand(5*self.n), method ='BFGS', tol =1e-3)