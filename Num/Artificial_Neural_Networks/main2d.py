import numpy as np
import ann2d

def f(x, y):                       #to train
    # x = input[0]
    # y = input[1]
    return y*x*np.exp(-x*x)*np.exp(-y*y)
def g(x, y):                       #to interpolate
    # x = input[0]
    # y = input[1]
    return np.cos(5*x)*np.exp(-x**2)*np.exp(-y*y)
# def h(x):
#     return np.exp(-x*x)
# def d(x):
#     return 3*x +2

if __name__ == '__main__':
    num_neurons = 20
    nw = ann2d.Neural_Network(num_neurons, f)
    x = np.linspace(-3, 3, 300)
    y = np.linspace(-3, 3, 300)
    gxy = g(x, y)
    nw.train(x, y, gxy)

    fy = nw.feed_forward(x, y)

    for i in range(y.size):
        print("{}   {}   {}   {}".format(x[i], y[i], fy[i], gxy[i]))
