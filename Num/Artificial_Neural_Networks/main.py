import numpy as np
import ann

def f(x):                       #to train
    return x*np.exp(-x**2)
def g(x):                       #to interpolate
    return np.cos(5*x)*np.exp(-x**2)
# def h(x):
#     return np.exp(-x*x)
# def d(x):
#     return 3*x +2

if __name__ == '__main__':
    num_neurons = 20
    nw = ann.Neural_Network(num_neurons, f)
    x = np.linspace(-3, 3, 100)
    gx = g(x)
    nw.train(x, gx)

    y = nw.feed_forward(x)

    for i in range(y.size):
        print("{}   {}   {}".format(x[i], y[i], gx[i]))

