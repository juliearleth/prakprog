import numpy as np
import scipy.optimize as opt


class Neural_Network:
    def __init__(self, num_neurons, function):
        self.n = num_neurons
        self.f = function
        self.params = np.empty([self.n, 3])

    def feed_forward(self, input):
        x =input
        output = 0
        for i in range(self.n):
            a = self.params[i][0]
            b = self.params[i][1]
            w = self.params[i][2]
            output += self.f((x + a)/b)*w
        return output

    def train(self, input_values:np.array, exact_values:np.array):
        def delta(p):
            self.params = np.reshape(p, (self.n, 3))
            f = exact_values
            y = self.feed_forward(input_values)
            s = np.sum(np.abs(y-f)**2)
            return s

        opt.minimize(delta, np.random.rand(3*self.n), method ='BFGS', tol =1e-3)