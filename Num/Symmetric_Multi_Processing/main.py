import sys
sys.path.append('../Monte_carlo_integration')
import mc_main as mc
import multiprocessing as mp
from time import time

if __name__ == '__main__':
    start = time()
    p1 = mp.Process(target = mc.main_A)
    p2 = mp.Process(target = mc.main_A)
    p3 = mp.Process(target = mc.main_A)

    p1.start()
    p2.start()
    p3.start()

    p1.join()
    p2.join()
    p3.join()

    stop = time()
    print("Normal Monte Carlo Integration processing time for main_A: ~{} s".format(16))
    print("processing time for three Monte Carlo Integration processes using multiprocessing on main_A: {} s".format((stop-start)))