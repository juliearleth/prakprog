import numpy as np
from math import *
import random

random.seed(2)
def SymMatrix(size):
    n = size
    A = np.empty((n, n))
    for i in range(n):
        A[i, i] = (i + 1) * random.random()
        for j in range(i + 1, n):
            A[i, j] = (i + j) * random.random()
            A[j, i] = A[i, j]
    return A


def jacobi_diagonalization(matrix, e,  V):
    A = matrix
    assert A.shape[0] == A.shape[1]
    n = A.shape[0]
    for i in range(n): e[i] = A[i, i]
    sweeps = 0
    changed = True
    while changed:
        sweeps += 1
        changed = False
        for q in reversed(range(n)):
            for p in range(q):
                app = e[p]
                aqq = e[q]
                apq = A[p, q]
                phi = (atan2(2 * apq, aqq - app))/2
                c = cos(phi)
                s = sin(phi)
                app1 = c * c * app - 2 * s * c * apq + s * s * aqq
                aqq1 = s * s * app + 2 * s * c * apq + c * c * aqq
                if app1 != app or aqq1 != aqq:
                    changed = True
                    e[p] = app1
                    e[q] = aqq1
                    A[p, q] = 0
                    for i in range(p):
                        aip = A[i, p]
                        aiq = A[i, q]
                        A[i, p] = c * aip - s * aiq
                        A[i, q] = s * aip + c * aiq
                    for i in range(p + 1, q):
                        api = A[p, i]
                        aiq = A[i, q]
                        A[p, i] = c * api - s * aiq
                        A[i, q] = s * api + c * aiq
                    for i in range(q + 1, n):
                        api = A[p, i]
                        aqi = A[q, i]
                        A[p, i] = c * api - s * aqi
                        A[q, i] = s * api + c * aqi
                    for i in range(n):
                        vip = V[i, p]
                        viq = V[i, q]
                        V[i, p] = c * vip - s * viq
                        V[i, q] = c * viq + s * vip
    return sweeps