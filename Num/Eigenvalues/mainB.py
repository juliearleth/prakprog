import sys
import numpy as np
import math
import jacobiB
from eigen import SymMatrix

if len(sys.argv)>1:
    n = int(sys.argv[1])
else:
    n = 4
if len(sys.argv)>2:
    N = int(sys.argv[2])
else:
    N = 3
max_print = 10

A = SymMatrix(n)
V = np.identity(n)
e = np.empty(n)

np.set_printoptions(precision=5)
if n>max_print:
    rotations = jacobiB.diag(A, e, V, N)
    print("e0 = {}, rotations = {}".format(e[0], rotations), file=sys.stderr)
else:
    print("\nJacobi diagonalization: several lowest eigenvalues:");
    print("----------------------");
    print("matrix size: {},  eigenvalues to calculate: {}".format(n, N))

    print("A random symmetric matrix A: \n{}".format(A))
    AA = A.copy()
    rotations = jacobiB.diag(A, e, V, N)
    print("\nrotations = {}".format(rotations))
    print("matrix A after diagonalization (first {} rows should be zeroed above diagonal): \n{}".format(N, A))
    print("\nvector e: first {} elements should be at the diagonal of V^T*A*V):\n{}".format(N, e))
    print("\nV^T*A*V (first {} rows/columns should be zeroed):\n{}".format(N, np.transpose(V) @ AA @ V));
