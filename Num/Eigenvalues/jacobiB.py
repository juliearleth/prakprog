from math import *


def approx(a: float, b: float, tiny: float = 1e-12):
    if (abs(a - b) < tiny): return True;
    if (abs(a - b) / (abs(a) + abs(b)) < tiny): return True
    return False


def diag(A, e, V, N: int = 1):
    assert A.shape[0] == A.shape[1]
    n = A.shape[0]
    assert N <= n and N > 0
    if N == n: N = n - 1
    for i in range(n): e[i] = A[i, i]
    rotations = 0
    for p in range(N):
        changed = True
        while changed:
            changed = False
            q = p + 1
            big = abs(A[p, q])
            for i in range(q + 1, n):
                big2 = abs(A[p, i])
                if big2 > big:
                    big = big2
                    q = i
            app = e[p]
            aqq = e[q]
            apq = A[p, q]
            phi = 0.5 * atan2(2 * apq, aqq - app)
            c = cos(phi)
            s = sin(phi)
            app1 = c * c * app - 2 * s * c * apq + s * s * aqq
            aqq1 = s * s * app + 2 * s * c * apq + c * c * aqq
            if not approx(app1, app):
                changed = True
                rotations += 1
                e[p] = app1
                e[q] = aqq1
                A[p, q] = 0
                for i in range(p + 1, q):
                    api = A[p, i]
                    aiq = A[i, q]
                    A[p, i] = c * api - s * aiq
                    A[i, q] = c * aiq + s * api
                for i in range(q + 1, n):
                    api = A[p, i]
                    aqi = A[q, i]
                    A[p, i] = c * api - s * aqi
                    A[q, i] = c * aqi + s * api
                for i in range(n):
                    vip = V[i, p]
                    viq = V[i, q]
                    V[i, p] = c * vip - s * viq
                    V[i, q] = c * viq + s * vip
    return rotations
