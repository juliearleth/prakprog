import numpy as np
import eigen
import sys

if __name__ == '__main__':
    if len(sys.argv) > 1:
        n = int(sys.argv[1])
    else:
        n = 4
    max_print = 10
    A = eigen.SymMatrix(n)
    V = np.identity(n)
    e = np.empty(n)
    np.set_printoptions(precision=5)

    if n > max_print:
        sweeps = eigen.jacobi_diagonalization(A, e, V);
        print("e0=", e[0], " sweeps=", sweeps, file=sys.stderr)
    else:
        print('Jacobi diagonalisation with cyclic sweeps')
        print('-----------------------------------------')
        print('n={}'.format(n))
        print('Random symmetric matrix A=\n{}'.format(A))
        AA = A.copy().astype(float)
        sweeps = eigen.jacobi_diagonalization(A, e, V)
        print('sweeps={}'.format(sweeps))
        print("A after diagonalization: \n{}".format(A))
        eigenvals_np = np.linalg.eigvalsh(AA, UPLO='U')
        print('The actual eigenvalues using numpy are: \n{}'.format(eigenvals_np))
        print('The calculated eigevalues are:  \n{}'.format(e))
        VTAV = np.transpose(V) @ AA @ V
        # VTAV = np.dot(np.dot(np.transpose(V), AA), V)
        print("VTAV = \n{}".format(VTAV))
