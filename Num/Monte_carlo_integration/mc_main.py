import mci
from math import *
from inspect import *
import numpy as np
from time import time


def f(p: list):
    x = p[0]
    y = p[1]
    if x * x + y * y < 1:
        return 1
    else:
        return 0

def diffi(p: list):
    x = p[0]
    y = p[1]
    z = p[2]
    return 1 / (1 - cos(x) * cos(y) * cos(z)) / pi / pi / pi



def main_B():
    global a, b, exact, N
    a= np.array([0, 0])
    b = np.array([1, 1])
    exact = pi / 4
    for n in range(1, 60):
        N = n * 1000
        (i, e) = mci.plainmc(f, a, b, N)
        print(N, e, abs(i - exact), file=sys.stderr)


def main_A():
    global a, b, exact, N
    a = np.array([0, 0])
    b = np.array([1, 1])
    exact = pi / 4
    N = 50000
    (integ, error) = mci.plainmc(f, a, b, N);
    print("Plain Monte-Carlo integration of the function\n")
    print(getsource(f));
    print("in a rectangular volume between", a, " and", b, "\n");
    print("plainmc: N                  =", N);
    print("plainmc: estimated integral =", integ);
    print("exact integral              =", exact);
    print("plainmc: estimated error    =", error);
    print("plainmc: actual error       =", abs(integ - exact));
    print();
    c = np.array([0, 0, 0])
    d = np.array([pi, pi, pi])
    exact = 1.3932039296856768591842462603255
    N = 500000
    (integ, error) = mci.plainmc(diffi, c, d, N);
    print("Plain Monte-Carlo integration of the function\n")
    print(getsource(diffi));
    print("in a rectangular volume between", c, " and", d, "\n");
    print("plainmc: N                  =", N);
    print("plainmc: estimated integral =", integ);
    print("exact integral              =", exact);
    print("plainmc: estimated error    =", error);
    print("plainmc: actual error       =", abs(integ - exact));
    print();



if __name__ == '__main__':
    start = time()
    main_A()
    # main_A()
    # main_A()
    stop = time()
    main_B()

    print("Processing time: {}".format((stop-start)))