from math import *
import numpy as np

def randomx(a:np.array, b:np.array):
    dim = a.size
    x = np.zeros(dim)
    for i in range(dim):
        x[i] = a[i] + np.random.rand(1) * (b[i] - a[i])
    return x

def plainmc(f:"function", a:np.array, b:np.array, N:int):
    dim = a.size
    V = 1
    for i in range(dim):
        V *= b[i] - a[i]
    sum = 0
    sum2 = 0
    for i in range(N):
        x = randomx(a, b) #array
        fx = f(x) #array
        sum += fx #array
        sum2 += fx*fx
    avr = sum/N
    var = sum2/N - avr*avr
    result = avr*V
    error = np.sqrt(var/N)*V
    return (result, error)