set terminal svg enhanced background rgb 'white'
set out 'out.A.errors.svg'
set logscale y
set xlabel 'N'
set ylabel 'error'
set title 'Plain Monte-Carlo integration error as function of number of samples N'
set logscale y
a=1
f(x)=a/sqrt(x)
fit f(x) 'out.A.errors.data' via a
plot 'out.A.errors.data' u 1:2 title 'estimated error' , 'out.A.errors.data' u 1:3 title 'actual error' , f(x) title '1/sqrt(N) fit'

