import numpy as np
import newton_min as mnewton
import qnewton
import sys
sys.path.append('../Root_finding')
import newton as rnewton

ncalls = 0

def rosenbrock(p):
    global ncalls
    ncalls += 1
    df = np.empty(2)
    x = p[0]; y = p[1]
    z = (1-x)**2 + 100*(y - x*x)**2
    df[0] = 2*(1-x)*(-1)+100*2*(y-x*x)*(-1)*2*x
    df[1] = 100*2*(y-x*x)
    H = np.empty([2, 2])
    # print("H={}".format(H))
    H[0, 0] = 1200*x*x - 400*y + 2
    # print("H={}".format(H))
    H[0, 1] = -400*x
    H[1, 0] = H[0, 1]
    H[1, 1] = 200
    return (z, df, H)


def qrosenbrock(p): #for quasi-Newton method
    global ncalls
    ncalls += 1
    x = p[0]; y = p[1]
    z = (1-x)**2 + 100*(y - x*x)**2
    return z

def rrosenbrock(p): #for root finding method
    global ncalls
    ncalls += 1
    df = np.empty(2)
    x = p[0];
    y = p[1]
    df[0] = 2 * (1 - x) * (-1) + 100 * 2 * (y - x * x) * (-1) * 2 * x
    df[1] = 100 * 2 * (y - x * x)
    return df

def himmelblau(p):
    global ncalls
    ncalls += 1
    df = np.empty(2)
    x = p[0]; y = p[1]
    z = (x*x + y - 11)**2 + (x + y*y - 7)**2
    df[0] = 2*(-7+x+y*y + 2*x*(-11 + x*x + y))
    df[1] = 2*(x*x + 2*y*(x + y*y -7) + y - 11)
    H = np.empty([2, 2])
    H[0, 0] = 12*x*x + 4*y - 42
    H[0, 1] = 4*(x + y)
    H[1, 0] = H[0, 1]
    H[1, 1] = 4*x + 12*y*y - 26
    return (z, df, H)

def qhimmelblau(p): #for quasi-Newton method
    global ncalls
    ncalls += 1
    x = p[0]; y = p[1]
    z = (x*x + y - 11)**2 + (x + y*y - 7)**2
    return z

def rhimmelblau(p): #for root finding method
    global ncalls
    ncalls += 1
    df = np.empty(2)
    x = p[0]; y = p[1]
    df[0] = 2*(-7+x+y*y + 2*x*(-11 + x*x + y))
    df[1] = 2*(x*x + 2*y*(x + y*y -7) + y - 11)
    return df

x0=np.array([5, 5])

print("--------------Minimization using analytic Hessian----------\n")

eps = 1e-8
print("Finding the minimum of the Rosenbrock function with a tolerance eps = {}:".format(eps))
mnewton.newton_time_print(rosenbrock, x0, eps)
print("Calls:             {}\n".format(ncalls))

ncalls = 0
print("Finding the minimum of the Himmelblau function with a tolerance eps = {}:".format(eps))
mnewton.newton_time_print(himmelblau, x0, eps)
print("Calls:             {}\n".format(ncalls))

print("--------------Minimization using Broyden's update----------\n")

ncalls = 0
dx = 1e-12
print("Finding the minimum of the Rosenbrock function using symmetric Broyden's update \nwith a tolerance eps = {}, and a step-size dx = {}:".format(eps, dx))
qnewton.qnewton_time_print(qrosenbrock, x0, dx, eps)
print("Calls:             {}\n".format(ncalls))

# x0=np.array([5, 5])
ncalls = 0
print("Finding the minimum of the Himmelblau function using symmetric Broyden's update \nwith a tolerance eps = {}, and a step-size dx = {}:".format(eps, dx))
qnewton.qnewton_time_print(qhimmelblau, x0, dx, eps)
print("Calls:             {}\n".format(ncalls))

print("--------------Minimization using Root finding----------\n")
ncalls = 0
print("Finding the minimum of the Rosenbrock function using root finding with a tolerance eps = {}, and a stepsize dx = {}".format(eps, dx))
rnewton.newton_time_print(rrosenbrock, x0, eps, dx)
print("Calls:             {}\n".format(ncalls))

ncalls = 0
print("Finding the minimum of the Himmelblau function using root finding with a tolerance eps = {}, and a stepsize dx = {}".format(eps, dx))
rnewton.newton_time_print(rhimmelblau, x0, eps, dx)
print("Calls:             {}\n".format(ncalls))

