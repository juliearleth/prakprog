import numpy as np
import time


def gradient(f, x:np.array, dx:float=1e-6):
    fx = f(x)
    dfdx = np.empty(x.size)
    for i in range(x.size):
        x[i] += dx
        dfdx[i] = (f(x) - fx)/dx
        x[i] -= dx
    return dfdx

def qnewton(f, xstart:np.array, dx:float=1e-6, eps:float=1e-7):
    x = xstart.copy().astype(float)
    dfdx = gradient(f, x, dx)
    fx = f(x)
    B = np.identity(x.size)
    epsilon = 1e-9
    alpha = 0.01
    steps = 0
    while True:
        steps += 1
        Dx = np.dot(B, -dfdx)
        lam = 2
        while True:
            lam /= 2.
            s = lam * Dx
            y = x + s
            fy = f(y)
            if np.abs(fy) < np.abs(fx) + alpha*np.dot(np.transpose(s), dfdx):
                break
            if  np.linalg.norm(s) < dx:
                B = np.identity(x.size)
                break
        dfy = gradient(f, y, dx)
        v = dfy - dfdx
        u = s - np.dot(B, v)

        if np.abs(np.dot(np.transpose(s), v)) > epsilon:
            gamma = np.divide(np.dot(np.transpose(u), v), 2 * np.dot(np.transpose(s), v))
            a = np.divide(u - gamma * s, np.dot(np.transpose(s), v))
            dB = np.outer(a, s) + np.outer(s, a)
            B += dB
        x = y
        fx = fy
        dfdx = dfy
        if np.linalg.norm(dfdx) < eps or np.linalg.norm(Dx) < dx: break
    return x, steps

def qnewton_time_print(f, xstart:np.array, dx:float, eps:float):
    start = time.time()
    (b, steps) = qnewton(f, xstart, dx, eps)
    stop = time.time()
    print("Initial vector x0: {}".format(xstart))
    print("f(x0):             {:20.15f}".format(f(xstart)))
    print("Solution x:        {}".format(b))
    print("f(x):              {:20.15f}".format(f(b)))
    # if np.all(f(b)[1]) < eps:
    print("Gradient(x)        {} < {}".format(gradient(f, b, dx), eps))

    print("Steps:             {}".format(steps))
    print("Time:              {}".format(stop - start))