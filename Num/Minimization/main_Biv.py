import numpy as np
import qnewton

t = np.array([0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77])
y = np.array([4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46])
e = np.array([0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24])

for i in range(t.size):
    print("{}   {}   {}".format(t[i], y[i], e[i]))

print("\n")

def decay(t, p):
    A = p[0]
    T = p[1]
    B = p[2]

    f = A * np.exp(-t/T) + B
    return f

def master(t, y, e, p):
    return np.sum(((decay(t, p)-y)**2)/(e*e))

fun = lambda p: master(t, y, e, p)

(params, steps) = qnewton.qnewton(fun, np.random.rand(3), dx=1e-10, eps=1e-8)

x = np.linspace(0, 10, 50)

for i in range(x.size):
    print("{}   {}".format(x[i], decay(x[i], params)))