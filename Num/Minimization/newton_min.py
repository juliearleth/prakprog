import sys, time
sys.path.append('../Linear_equations')
import gramschmidt as gs
import numpy as np



def newton(f:"function", xstart:np.array, eps:float):
    x = xstart.copy().astype(float)
    alpha = 0.01
    steps = 0
    while True:
        steps += 1
        Hx = f(x)[2]
        # print("f(x)={}".format(f(x)))
        # break
        dfx = f(x)[1]
        fx = f(x)[0]
        (Q, R) = gs.qr(Hx)
        Dx = gs.qr_solve(Q, R, -dfx)
        lam = 2.
        while True:
            lam /= 2.
            s = lam * Dx
            y = x + s
            fy = f(y)[0]
            dfy = f(y)[1]
            if fy < fx + alpha*np.dot(np.transpose(s), dfx) or lam < 1./64.:
                # print("ok")
                break
        x = y
        dfx = dfy
        if np.linalg.norm(dfx) < eps: break
    return x, steps

def newton_time_print(f:"function", xstart:np.array, eps:float):
    start = time.time()
    (b, steps) = newton(f, xstart, eps)
    stop = time.time()
    print("Initial vector x0: {}".format(xstart))
    print("f(x0):             {:20.15f}".format(f(xstart)[0]))
    print("Solution x:        {}".format(b))
    print("f(x):              {:20.15f}".format(f(b)[0]))
    # if np.all(f(b)[1]) < eps:
    print("Gradient(x)        {} < {}".format(f(b)[1], eps))

    print("Steps:             {}".format(steps))
    print("Time:              {}".format(stop - start))