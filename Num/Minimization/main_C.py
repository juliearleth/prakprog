import sys, math
from random import random
import numpy as np
import downhill_simplex as ds

ncalls = 0


def Himmelblau(p):
    global ncalls
    ncalls += 1
    x = p[0]
    y = p[1]
    z = math.pow(x * x + y - 11, 2) + math.pow(x + y * y - 7, 2)
    return z


def Rosenbrock(p: np.array):
    global ncalls
    ncalls += 1
    assert p.size == 2
    x = p[0]
    y = p[1]
    return math.pow(1 - x, 2) + 100 * math.pow(y - x * x, 2)


def main_C1():
    simplex_size_goal = 1e-4
    p0 = np.array([18, 15])
    p1 = p0 + np.array([random(), random()])
    p2 = p0 + np.array([random(), random()])
    simplex = [p0, p1, p2]
    print("downhill simplex method: finding minimum of Rosenbrock function")
    print("starting around this point: {}".format(p0))
    s = ds.downhill(Rosenbrock, simplex, simplex_size_goal)
    print("ncalls= {}".format(ncalls))
    print("solution : {}".format(s))
    print("f(solution)= {}".format(Rosenbrock(s)))
    p0 = np.array([18, 15])
    p1 = p0 + np.array([random(), random()])
    p2 = p0 + np.array([random(), random()])
    simplex = [p0, p1, p2]
    print("\ndownhill simplex method: finding minimum of Himmelblau function")
    print("starting around this point: {}".format(p0))
    s = ds.downhill(Himmelblau, simplex, simplex_size_goal)
    print("ncalls= {}".format(ncalls))
    print("solution : {}".format(s))
    print("f(solution)= {}".format(Himmelblau(s)))

def main_C2():
    t = np.array([0.23, 1.29, 2.35, 3.41, 4.47, 5.53, 6.59, 7.65, 8.71, 9.77])
    y = np.array([4.64, 3.38, 3.01, 2.55, 2.29, 1.67, 1.59, 1.69, 1.38, 1.46])
    e = np.array([0.42, 0.37, 0.34, 0.31, 0.29, 0.27, 0.26, 0.25, 0.24, 0.24])

    for i in range(t.size):
        print("{}   {}   {}".format(t[i], y[i], e[i]))

    print("\n")

    def decay(t, p):
        A = p[0]
        T = p[1]
        B = p[2]

        f = A * np.exp(-t / T) + B
        return f

    def master(t, y, e, p):
        return np.sum(((decay(t, p) - y) ** 2) / (e * e))

    fun = lambda p: master(t, y, e, p)
    simplex_size_goal = 1e-4
    p0 = np.array([18, 15, 8])
    p1 = p0 + np.array([random(), random(), random()])
    p2 = p0 + np.array([random(), random(), random()])
    simplex = [p0, p1, p2]
    params = ds.downhill(fun, simplex, simplex_size_goal)

    x = np.linspace(0, 10, 50)

    for i in range(x.size):
        print("{}   {}".format(x[i], decay(x[i], params)))




if __name__ == '__main__':
    # main_C1()
    main_C2()