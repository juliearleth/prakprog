import numpy as np
import sys


def reflect(highest: np.array, centroid: np.array):
    return 2 * centroid - highest


def expand(highest: np.array, centroid: np.array):
    return 3 * centroid - 2 * highest


def contract(highest: np.array, centroid: np.array):
    return centroid + 0.5 * (highest - centroid)


def reduce(simplex: list, ilow: int):
    for k in range(len(simplex)):
        if k != ilow: simplex[k] = 0.5 * (simplex[k] + simplex[ilow])


def size(simplex: list):
    s = np.linalg.norm(simplex[0] - simplex[-1])
    for k in range(1, len(simplex)):
        d = np.linalg.norm(simplex[k] - simplex[k - 1])
        if s < d: s = d
    return s


def update(simplex: list, fs: np.array, centroid: np.array):
    ihigh = 0;
    fhigh = fs[0]
    ilow = 0;
    flow = fs[0]
    for k in range(1, len(simplex)):
        if fs[k] > fhigh:
            fhigh = fs[k]
            ihigh = k
        if fs[k] < flow:
            flow = fs[k]
            ilow = k
    s = np.zeros(simplex[0].size)
    for k in range(len(simplex)):
        if k != ihigh: s += simplex[k]
    for i in range(s.size): centroid[i] = s[i] / (len(simplex) - 1)
    return (ilow, ihigh)


def initiate(simplex: list, F: 'objective function', fs: np.array):
    for i in range(len(simplex)): fs[i] = F(simplex[i])


def downhill(F: 'objective function', simplex: list, simplex_size_goal: float = 1e-3):
    fs = np.empty(len(simplex))
    centroid = np.empty(len(simplex) - 1)
    initiate(simplex, F, fs)
    while size(simplex) > simplex_size_goal:
        (ilow, ihigh) = update(simplex, fs, centroid)
        # do reflection
        r = reflect(simplex[ihigh], centroid)
        fr = F(r)
        if fr < fs[ilow]:  # try expansion
            e = expand(simplex[ihigh], centroid)
            fe = F(e)
            if fe < fr:  # accept expansion
                print("expansion", file=sys.stderr)
                simplex[ihigh] = e
                fs[ihigh] = fe
            else:  # reject expansion, accept reflection
                print("reflection", file=sys.stderr)
                simplex[ihigh] = r
                fs[ihigh] = fr
        else:  # reflection wasn't too good
            if fr < fs[ihigh]:  # accept reflection
                print("reflection", file=sys.stderr)
                simplex[ihigh] = r
                fs[ihigh] = fr
            else:  # try contraction
                c = contract(simplex[ihigh], centroid)
                fc = F(c)
                if fc < fs[ihigh]:  # accept contraction
                    print("contraction", file=sys.stderr)
                    simplex[ihigh] = c
                    fs[ihigh] = fc
                else:  # reduction
                    print("reduction", file=sys.stderr)
                    reduce(simplex, ilow)
                    initiate(simplex, F, fs)
    return simplex[ilow]
