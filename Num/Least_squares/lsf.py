import sys
sys.path.append('../Linear_equations')
import gramschmidt
import numpy as np

def lsfit(fs:np.array, x:np.array, y:np.array, dy:np.array) :
    n = x.size; m = len(fs)
    A = np.empty([n, m])
    b = np.empty(n)
    for i in range(n):
        b[i] = y[i]/dy[i]
        for k in range(m):
            A[i, k] = fs[k](x[i])/dy[i]
    QR_both = gramschmidt.qr(A)
    c = gramschmidt.qr_solve(QR_both[0], QR_both[1], b)
    inverse_R = np.linalg.inv(QR_both[1])
    S = inverse_R * np.transpose(inverse_R)
    return c, S


