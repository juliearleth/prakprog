from __future__ import print_function
import math, sys
import numpy as np
from lsf import lsfit
import random


def p0(z): return np.log(z)
def p1(z): return 1.0
def p2(z): return z
def p3(z): return math.pow(z, 0)
def p4(z): return math.pow(z, 1)
def p5(z): return math.pow(z, 2)
def p6(z): return math.pow(z, 3)


def fit_print(x, y, dy, funs:list):
    n = x.size
    m = len(funs)
    for i in range(n):
        print(x[i], y[i], dy[i], end="\n")
    print("\n")
    (c, S) = lsfit(funs, x, y, dy)
    dc = np.empty(m)
    for i in range(m):
        dc[i] = math.sqrt(S[i, i])
    print("The covariance matrix S=\n {}\n The errors of the fitting coefficients: \n{}\n\n".format(S, dc))

    def fit(x):
        s = 0
        for i in range(m): s += c[i] * funs[i](x)
        return s

    def df(x):
        sum = 0
        for i in range(m):
            for j in range(m):
                sum += funs[i](x) * S[i, j] * funs[j](x)
        return math.sqrt(sum)

    dz = (x[-1] - x[0]) / 90
    z = x[0] - dz / 2
    while z < x[-1] + dz:
        print(z, fit(z), fit(z) + df(z), fit(z) - df(z), end="\n")
        z += dz
    print("\n")

def main_A_Bi():
    global x, c, S
    x = np.array([0.1, 1.33, 2.55, 3.78, 5, 6.22, 7.45, 8.68, 9.9])
    y = np.array([-15.3, 0.32, 2.45, 2.75, 2.27, 1.35, 0.157, -1.23, -2.75])
    dy = np.array([1.04, 0.594, 0.983, 0.998, 1.11, 0.398, 0.535, 0.968, 0.478])

    funs = [p0, p1, p2]

    fit_print(x, y, dy, funs)

def main_Bii(fun_to_fit, funs:list):
    n = 10
    x = np.empty(n)
    y = np.empty(n)
    dy = np.empty(n)
    a = -0.9; b = 0.9

    for i in range(n): #generate data
        x[i] = a + (b - a)*i/(n-1)
        y[i] = fun_to_fit(x[i]) + (random.random() - 0.5)
        dy[i] = 0.1 + random.random()
    fit_print(x, y, dy, funs)

if __name__ == '__main__':
    funs0 = [p3, p4, p5]
    funs1 = [p3, p4, p5, p6]

    def fun0(z): return 1 + 2 * z + 3 * z * z
    def fun1(z): return -2 + 4*z + 2*z*z - 3*z*z*z

    if sys.argv[1:][0] == 'A':
        main_A_Bi()
    elif sys.argv[1:][0] == 'B':
        main_Bii(fun0, funs0)
    elif sys.argv[1:][0] == 'B2':
        main_Bii(fun1, funs1)
    else:
        for arg in sys.argv[1:]:
            print("Assignment '{}' not recognized".format(arg))
