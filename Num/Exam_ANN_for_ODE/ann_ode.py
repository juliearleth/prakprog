import numpy as np
import scipy.optimize as opt

class Neural_Network:
     def __init__(self, num_neurons, activation_function):
         self.n = num_neurons
         self.g = activation_function[0]
         self.dg = activation_function[1]
         self.params = np.empty([self.n, 3])

     def feed_forward(self, input):
         x = input
         output_F = 0
         output_dF = 0
         for i in range(self.n):
             a = self.params[i][0]
             b = self.params[i][1]
             w = self.params[i][2]
             output_F  += w*self.g((x-a)/b)
             output_dF += w*self.dg((x-a)/b)/b
         return (output_F, output_dF)

     def train(self, input_values:np.array, training_fun:'function', init:np.array):
         def delta(p):
             self.params = np.reshape(p, (self.n, 3))
             f = training_fun
             y = self.feed_forward(input_values)
             last_term = input_values.size * np.abs(self.feed_forward(init[0])[0]-init[1])**2
             s = np.sum(np.abs(y[1]-f(input_values, y[0]))**2) + last_term
             return s



         opt.minimize(delta, np.reshape(self.params, [1, 3*self.n]), method='BFGS', tol=1e-6)
