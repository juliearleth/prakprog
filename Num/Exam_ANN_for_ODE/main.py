import numpy as np
import ann_ode as ann


def logistic(x):  # exact function
    return np.divide(1, 1+np.exp(-x))
def dlogistic(x):
    return logistic(x) * (1 - logistic(x))
log = [logistic, dlogistic]


def gauss(x):  # exact function
    return np.exp(-x * x / 2)
def dgauss(x):
    return -x * gauss(x)
gauss_list = [gauss, dgauss]


def logistic_train(x, y): #train
    return y*(1-y)


def gauss_train(x, y): #train
    return -x*y


def xvals(a, b, N):
    x = np.empty(N)
    for i in range(N):
        x[i] = a + (b-a)*(i)/(N)
    return x

if __name__ == '__main__':
    num_neurons = 20
    N=50
    x = xvals(-5, 5, N)
    nw = ann.Neural_Network(num_neurons, log)

    p = np.empty([num_neurons, 3])

    for i in range(num_neurons):
        p[i][0] = -5 + (5 - (-5)) * i
        p[i][1] = 1
        p[i][2] = 1
    nw.params = p

    nw.train(x, logistic_train, np.array([0, 0.5]))

    (y, dy) = nw.feed_forward(x)
    # print("x={}".format(x))
    # print("y={}".format(y))

    for i in range(x.size):
        print("{}   {}   {}".format(x[i], y[i], logistic(x[i])))

    nw2 = ann.Neural_Network(num_neurons, gauss_list)

    p = np.empty([num_neurons, 3])

    for i in range(num_neurons):
        p[i][0] = -5 + (5 - (-5)) * i/(num_neurons-1)
        p[i][1] = 1
        p[i][2] = 1
    nw2.params = p

    nw2.train(x, gauss_train, np.array([0, 1]))
    (y2, dy2) = nw2.feed_forward(x)

    # print(y2)
    print("\n")

    for i in range(x.size):
        print("{}   {}   {}".format(x[i], y2[i], gauss(x[i])))